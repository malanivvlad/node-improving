const logger = require('../utils/logger');

module.exports = function asyncWrap (callback) {
  return function (req, res, next) {
    try {
      callback(req, res, next);
    } catch (e) {
      logger.error(e.message);
    }
  }
}