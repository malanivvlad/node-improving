const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground', {useNewUrlParser: true})
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const authorSchema = new mongoose.Schema({
  name: String,
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  authors: [authorSchema]
}));

async function createCourse(name, authors) {
  const course = new Course({
    name,
    authors
  });

  const result = await course.save();
  console.log(result);
}

async function listCourses() {
  const courses = await Course.find();
  console.log(courses);
}

async function updateAuthor(courseId) {
  const course = await Course.updateOne({_id: courseId}, {
    $set: {
      'author.name': " dev"
    }
  });
}

async function unsetAuthor(courseId) {
  const course = await Course.updateOne({_id: courseId}, {
    $unset: {
      'author': ""
    }
  });
}

async function addAuthor(courseId, author) {
  const course = await Course.findById(courseId);
  course.authors.push(author);
  course.save();
}

async function removeAuthor(courseId, authorId)
{
  const course = await Course.findById(courseId);
  const author = course.authors.id(authorId);
  author.remove();
  course.save();
}
// createCourse('Node ', [
//   new Author({name: 'Den'}),
//   new Author({name: 'Vlad'})]);
//updateAuthor('5d4d592d7da5103da81dd206');
//unsetAuthor('5d4d592d7da5103da81dd206')
//addAuthor('5d4d68ebafce894d1a6a4b9c', new Author({name: 'Ban'}));
removeAuthor("5d4d68ebafce894d1a6a4b9c", "5d4d68ebafce894d1a6a4b9a" )