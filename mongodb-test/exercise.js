const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/mongo-exercises', {useNewUrlParser: true})
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.log('Could not connect to MongoDB...', err));
const courseSchema = new mongoose.Schema({
  name:String,
  author:String,
  tags:[String],
  date:Date,
  isPublished:Boolean,
  price:Number
})
const Course = mongoose.model('Course', courseSchema);

async function getCourses1() {
  const courses = await Course
    .find({isPublished:true, tags:'backend'})
    .select({name:1, author:1});
  console.log(courses);
}

//getCourses1();
async function getCourses2() {
  const courses = await Course
    .find({isPublished:true})
    .or([{tags:'backend'}, {tags:'frontend'}])
    .sort({name:-1})
    .select({name:1, author:1});
  console.log(courses);
}
getCourses2()
async function getCourses3() {
  const courses = await Course
    .find({ isPublished: true })
    .or( [{ price: { $gte: 15 } }, { name: /.*by.*/i }])
    .sort('-price')
    .select('name author price');

  console.log(courses);
}
getCourses3()