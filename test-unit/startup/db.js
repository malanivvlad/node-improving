const winston = require('winston');
const mongoose = require('mongoose');
const config = require('../config/test')

module.exports = function() {
  mongoose.connect(config.db)
    .then(() => winston.info(`Connected to ${config.db}...`));
}