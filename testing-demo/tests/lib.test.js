const lib = require('../lib');
const db = require('../db')
const mail = require('../mail')
// describe('absolute', () => {
//   it('should return pos num', () => {
//     const result = lib.absolute(1);
//     expect(result).toBe(1)
//   })
//   it('should return pos num with neg input', () => {
//     const result = lib.absolute(-1);
//     expect(result).toBe(1)
//   })
//   it('should return 0', () => {
//     const result = lib.absolute(0);
//     expect(result).toBe(0)
//   })
// })
// describe('greet', () => {
//   it('should return greet msg', () => {
//     const res = lib.greet('Vlad');
// // expect(res).toMatch(/Vlad/);
//     expect(res).toContain('Vlad');
//
//   })
//
// })
// describe('getCurrencies', () => {
//   it('should return sup currs', () => {
//     const res = lib.getCurrencies();
//
//     expect(res).toBeDefined();
//     expect(res).not.toBeNull();
//
//     expect(res[0]).toBe('USD')
//     expect(res[1]).toBe('AUD')
//     expect(res[2]).toBe('EUR')
//     expect(res.length).toBe(3);
//
//
//     expect(res).toContain('USD')
//     expect(res).toContain('EUR')
//     expect(res).toContain('USD')
//
//     expect(res).toEqual(expect.arrayContaining(['AUD', 'USD', 'EUR']))
//   })
//
// })
// describe('getProduct', () => {
//   it('should return the product with the given id', () => {
//     const res = lib.getProduct(1);
//    // expect(res).toEqual({id:1, price:10});
//
//     expect(res).toMatchObject({id:1, price:10});
//     expect(res).toHaveProperty('id', '1');
//
//   })
// describe('registerUser', () => {
//   it('should throw if username is falsy', () => {
//     const args = [null, undefined, NaN, '', 0, false];
//     args.forEach(a=>{
//       expect(()=>{lib.registerUser(a)}).toThrow
//     })
//   })
//   it('should return a user obj if valid username is passed',()=>{
//
//    const res =  lib.registerUser("Vladik")
//     expect(res).toMatchObject({username:'Vladik'})
//     expect(res.id).toBeGreaterThan(0)
//
//   })
// })
describe('applyDiscount', () => {
  it('should apply 10% discount if customer has more 10 points', () => {
    db.getCustomerSync = function (customerId) {
      console.log('Fake reading customer...')
      return {id: customerId, points: 20}
    }
    const order = {customerId: 1, totalPrice: 10}
    lib.applyDiscount(order);
    expect(order.totalPrice).toBe(9)
  })
})
describe('notifyCustomer', () => {
  it('should send an email to the customer', () => {
     //const mockFunction = jest.fn();
    // mockFunction.mockReturnValue(1)
   // mockFunction.mockResolvedValue(1)
  //  mockFunction.mockRejectedValue(new Error('...'))
    db.getCustomerSync = jest.fn().mockReturnValue({email:'a'})

    mail.send = jest.fn()
    lib.notifyCustomer({customerId: 1})
    expect(mail.send).toHaveBeenCalled()
    expect(mail.send.mock.calls[0][0]).toBe('a')
    expect(mail.send.mock.calls[0][1]).toMatch(/order/)

  })


})