module.exports.FooBar = function (input) {
  if (typeof input !== 'number')
    throw new Error('Input should be a number.');
  if ((input % 3 === 0) && (input % 5) === 0)
    return 'FooBar'
  if (input % 3 === 0)
    return 'Foo'
  if (input % 5 === 0)
    return 'Bar'
  return input
}
