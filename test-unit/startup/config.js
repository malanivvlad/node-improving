const config = require('../config/custom-environment-variables');

module.exports = function() {
  if (!config.jwtPrivateKey) {
    throw new Error('FATAL ERROR: jwtPrivateKey is not defined.');
  }
}