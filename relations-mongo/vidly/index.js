//require('express-async-errors');
const winston = require('winston');
const error  = require('./middleware/error');
const logger = require('./utils/logger');
const config = require('./config/custom-environment-variables')
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
const customers = require('./routes/customers');
const rentals  = require('./routes/rentals');
const genres  = require('./routes/genres');
const movies = require('./routes/movies');
const users = require('./routes/users');
const auth = require('./routes/auth');
const express = require('express');
const app = express();


process.on('unhandledRejection', error=>{
  logger.error(error.message);
})


if(!config.jwtPrivateKey){
  console.error('FATAL ERROR: jwtPrivate is not defined.');
  process.exit(1);
}

mongoose.connect('mongodb://localhost/vidly')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...'));

app.use(express.json());
app.use('/api/customers', customers);
app.use('/api/rentals', rentals);
app.use('/api/genres', genres);
app.use('/api/movies', movies);
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use(error);


const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));