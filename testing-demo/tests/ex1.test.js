const ex = require('../exercise1')

describe('exercise', () => {
  it('Error', () => {
    expect(() => {ex.FooBar('a').toThrow()})
    expect(() => {ex.FooBar(null).toThrow()})
    expect(() => {ex.FooBar(undefined).toThrow()})
    expect(() => {ex.FooBar({}).toThrow()})

  })
  // it('should return FooBar', () => {
  //   const result = ex.FooBar(15);
  //   expect(result).toBe('FooBar')
  // })
  // it('should return Foo', () => {
  //   const result = ex.FooBar(3);
  //   expect(result).toBe('Foo')
  // })
  // it('should return Bar', () => {
  //   const result = ex.FooBar(5);
  //   expect(result).toBe('Bar')
  // })
  // it('should return input', () => {
  //   const result = ex.FooBar(17);
  //   expect(result).toBe(17)
  // })
})