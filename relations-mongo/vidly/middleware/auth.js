const jwt = require('jsonwebtoken');
const config = require('../config/custom-environment-variables');

module.exports = function (req, res, next) {
  const token = req.headers['x-auth-token'];
  if (!token) return res.status(401).send('Access denied. No token provided');
  try {
    req.user = jwt.verify(token, config.jwtPrivateKey);
    next();
  }
  catch (ex) {
    res.status(400).send('Invalid token.')
  }
}
